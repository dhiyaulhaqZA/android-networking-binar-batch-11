package id.logivity.binarretrofit.network.model

import com.google.gson.annotations.SerializedName

data class StudentBody(
    @SerializedName("email")
    val email: String?,
    @SerializedName("name")
    val name: String?
)