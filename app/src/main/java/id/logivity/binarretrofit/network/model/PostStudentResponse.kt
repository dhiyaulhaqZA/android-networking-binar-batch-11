package id.logivity.binarretrofit.network.model

import com.google.gson.annotations.SerializedName

data class PostStudentResponse(
    @SerializedName("data")
    val data: StudentResult?,
    @SerializedName("error")
    val error: ErrorResponse?,
    @SerializedName("status")
    val status: String?
)