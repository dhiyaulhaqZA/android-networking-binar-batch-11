package id.logivity.binarretrofit.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiClient {
    private val retrofit: Retrofit
        get() {
            return Retrofit.Builder()
                .baseUrl("https://kotlinspringcrud.herokuapp.com/api/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }

    private val logging: HttpLoggingInterceptor
        get() {
            return HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
        }

    private val client: OkHttpClient
        get() {
            return OkHttpClient.Builder()
                .addInterceptor(logging)
                .build()
        }

    fun getApiService(): ApiService {
        return retrofit.create(ApiService::class.java)
    }
}