package id.logivity.binarretrofit.network.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class StudentResponse(
    @SerializedName("data")
    val data: List<StudentResult>?,
    @SerializedName("error")
    val error: ErrorResponse?,
    @SerializedName("status")
    val status: String?
)


data class StudentResult(
    @SerializedName("email")
    val email: String?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("name")
    val name: String?
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(email)
        parcel.writeValue(id)
        parcel.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<StudentResult> {
        override fun createFromParcel(parcel: Parcel): StudentResult {
            return StudentResult(parcel)
        }

        override fun newArray(size: Int): Array<StudentResult?> {
            return arrayOfNulls(size)
        }
    }
}

data class ErrorResponse(
    @SerializedName("code")
    val code: Int?,
    @SerializedName("message")
    val message: String?
)