package id.logivity.binarretrofit.network.model

import com.google.gson.annotations.SerializedName

data class DeleteStudentResponse(
    @SerializedName("data")
    val data: Any?,
    @SerializedName("error")
    val error: Any?,
    @SerializedName("status")
    val status: String?
)