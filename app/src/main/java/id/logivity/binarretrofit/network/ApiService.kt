package id.logivity.binarretrofit.network

import id.logivity.binarretrofit.network.model.DeleteStudentResponse
import id.logivity.binarretrofit.network.model.PostStudentResponse
import id.logivity.binarretrofit.network.model.StudentBody
import id.logivity.binarretrofit.network.model.StudentResponse
import retrofit2.Call
import retrofit2.http.*

interface ApiService {
    @GET("v1/student/all")
    fun getStudentList(): Call<StudentResponse>

    @POST("v1/student/")
    fun postStudent(@Body studentBody: StudentBody): Call<PostStudentResponse>

    @PUT("v1/student/{id}")
    fun editStudent(@Path("id") studentId: Int,
                    @Body studentBody: StudentBody): Call<PostStudentResponse>

    @DELETE("v1/student/{id}")
    fun deleteStudent(@Path("id") studentId: Int): Call<DeleteStudentResponse>
}