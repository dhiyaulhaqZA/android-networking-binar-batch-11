package id.logivity.binarretrofit.features.editor

import id.logivity.binarretrofit.network.ApiClient
import id.logivity.binarretrofit.network.model.DeleteStudentResponse
import id.logivity.binarretrofit.network.model.PostStudentResponse
import id.logivity.binarretrofit.network.model.StudentBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class StudentEditorPresenter(private val listener: Listener) {

    fun deleteStudent(studentId: Int) {
        listener.onAddStudentLoading(true)
        val call = ApiClient.getApiService().deleteStudent(studentId)
        call.enqueue(object : Callback<DeleteStudentResponse> {
            override fun onFailure(call: Call<DeleteStudentResponse>, t: Throwable) {
                listener.onAddStudentLoading(false)
                listener.onAddStudentFailure("Something went wrong")
            }

            override fun onResponse(call: Call<DeleteStudentResponse>, response: Response<DeleteStudentResponse>) {
                listener.onAddStudentLoading(false)
                listener.onAddStudentSuccess()
            }

        })
    }

    fun editStudent(studentId: Int, studentBody: StudentBody) {
        listener.onAddStudentLoading(true)
        val call = ApiClient.getApiService().editStudent(studentId, studentBody)
        call.enqueue(object : Callback<PostStudentResponse> {
            override fun onFailure(call: Call<PostStudentResponse>, t: Throwable) {
                listener.onAddStudentLoading(false)
                listener.onAddStudentFailure("Something went wrong")
            }

            override fun onResponse(call: Call<PostStudentResponse>, response: Response<PostStudentResponse>) {
                listener.onAddStudentLoading(false)
                val body = response.body()
                val code = response.code()
                if (body != null && code == 200) {
                    val data = body.data
                    if (data != null) {
                        listener.onAddStudentSuccess()
                    }
                    return
                }
                listener.onAddStudentFailure("Something went wrong")
            }
        })
    }

    fun addStudent(studentBody: StudentBody) {
        listener.onAddStudentLoading(true)
        val call = ApiClient.getApiService().postStudent(studentBody)
        call.enqueue(object : Callback<PostStudentResponse> {
            override fun onFailure(call: Call<PostStudentResponse>, t: Throwable) {
                listener.onAddStudentLoading(false)
                listener.onAddStudentFailure("Something went wrong")
            }

            override fun onResponse(call: Call<PostStudentResponse>, response: Response<PostStudentResponse>) {
                listener.onAddStudentLoading(false)
                val body = response.body()
                val code = response.code()
                if (body != null && code == 200) {
                    val data = body.data
                    if (data != null) {
                        listener.onAddStudentSuccess()
                    }
                    return
                }
                listener.onAddStudentFailure("Something went wrong")
            }
        })
    }

    interface Listener {
        fun onAddStudentSuccess()
        fun onAddStudentFailure(errMessage: String)
        fun onAddStudentLoading(isLoading: Boolean)
    }
}