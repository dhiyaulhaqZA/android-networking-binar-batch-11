package id.logivity.binarretrofit.features.home

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.logivity.binarretrofit.R
import id.logivity.binarretrofit.network.model.StudentResult
import kotlinx.android.synthetic.main.item_student.view.*

class HomeAdapter(private val listener: Listener): RecyclerView.Adapter<HomeAdapter.ViewHolder>() {

    private val studentList = mutableListOf<StudentResult>()

    fun addStudentList(students: MutableList<StudentResult>) {
        studentList.clear()
        studentList.addAll(students)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_student, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return studentList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }


    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                listener.onStudentItemClicked(studentList[adapterPosition])
            }
        }

        fun bind(position: Int) {
            val student = studentList[position]
            itemView.tv_item_student_email.text = student.email
            itemView.tv_item_student_name.text = student.name
        }

    }

    interface Listener {
        fun onStudentItemClicked(studentResult: StudentResult)
    }
}