package id.logivity.binarretrofit.features.home

import id.logivity.binarretrofit.network.ApiClient
import id.logivity.binarretrofit.network.model.StudentResponse
import id.logivity.binarretrofit.network.model.StudentResult
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomePresenter(private val listener: Listener) {

    fun getStudentList() {
        listener.onGetStudentListLoading(true)
        val call = ApiClient.getApiService().getStudentList()
        call.enqueue(object : Callback<StudentResponse> {
            override fun onFailure(call: Call<StudentResponse>, t: Throwable) {
                listener.onGetStudentListLoading(false)
                listener.onGetStudentListFailure("Something went wrong")
            }

            override fun onResponse(call: Call<StudentResponse>, response: Response<StudentResponse>) {
                listener.onGetStudentListLoading(false)
                val body = response.body()
                val code = response.code()
                if (body != null && code == 200) {
                    val data = body.data
                    if (data != null) {
                        listener.onGetStudentListSuccess(data.toMutableList())
                    }
                    return
                }
                listener.onGetStudentListFailure("Something went wrong")
            }
        })
    }

    interface Listener {
        fun onGetStudentListSuccess(studentList: MutableList<StudentResult>)
        fun onGetStudentListFailure(errMessage: String)
        fun onGetStudentListLoading(isLoading: Boolean)
    }
}