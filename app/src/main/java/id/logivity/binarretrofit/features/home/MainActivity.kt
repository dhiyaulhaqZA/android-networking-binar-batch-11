package id.logivity.binarretrofit.features.home

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import id.logivity.binarretrofit.R
import id.logivity.binarretrofit.config.Config
import id.logivity.binarretrofit.features.editor.StudentEditorActivity
import id.logivity.binarretrofit.network.model.StudentResult
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), HomePresenter.Listener, HomeAdapter.Listener {

    private lateinit var homePresenter: HomePresenter
    private lateinit var homeAdapter: HomeAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupComponent()
        setupListener()
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            homePresenter.getStudentList()
        }
    }

    private fun setupComponent() {
        homePresenter = HomePresenter(this)
        homeAdapter = HomeAdapter(this)

        rv_main.setHasFixedSize(true)
        rv_main.layoutManager = LinearLayoutManager(this)
        rv_main.adapter = homeAdapter

        homePresenter.getStudentList()
    }

    private fun setupListener() {
        fab_main.setOnClickListener {
            val intent = Intent(this, StudentEditorActivity::class.java)
            startActivityForResult(intent, Config.RC_STUDENT_ACTION)
        }
    }

    override fun onGetStudentListSuccess(studentList: MutableList<StudentResult>) {
        homeAdapter.addStudentList(studentList)
    }

    override fun onGetStudentListFailure(errMessage: String) {
        Toast.makeText(this, errMessage, Toast.LENGTH_SHORT).show()
    }

    override fun onGetStudentListLoading(isLoading: Boolean) {
        pb_main.visibility = if (isLoading) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    override fun onStudentItemClicked(studentResult: StudentResult) {
        val intent = Intent(this, StudentEditorActivity::class.java)
        intent.putExtra(Config.STUDENT_DATA_KEY, studentResult)
        startActivityForResult(intent, Config.RC_STUDENT_ACTION)
    }

}
