package id.logivity.binarretrofit.features.editor

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import id.logivity.binarretrofit.R
import id.logivity.binarretrofit.config.Config
import id.logivity.binarretrofit.network.model.StudentBody
import id.logivity.binarretrofit.network.model.StudentResult
import kotlinx.android.synthetic.main.activity_student_editor.*

class StudentEditorActivity : AppCompatActivity(), StudentEditorPresenter.Listener {

    private lateinit var editorPresenter: StudentEditorPresenter
    private lateinit var progressDialog: ProgressDialog // for example
    private var selectedStudent: StudentResult? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_student_editor)
        title = getString(R.string.title_add)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setupComponent()
        setupListener()
        writeComponent()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_editor, menu)
        val menuItem = menu?.findItem(R.id.action_delete)
        val isVisible = selectedStudent != null
        menuItem?.isVisible = isVisible
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.action_delete -> {
                editorPresenter.deleteStudent(selectedStudent?.id?: return true)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setupComponent() {
        editorPresenter = StudentEditorPresenter(this)
        progressDialog = ProgressDialog(this)
        progressDialog.setMessage(getString(R.string.msg_loading))
    }

    private fun writeComponent() {
        selectedStudent = intent.getParcelableExtra(Config.STUDENT_DATA_KEY)
        selectedStudent?.let {
            title = getString(R.string.title_edit)
            et_student_editor_email.setText(it.email)
            et_student_editor_name.setText(it.name)
            return
        }

        invalidateOptionsMenu()
    }

    private fun setupListener() {
        btn_student_editor_save.setOnClickListener {
            val email = et_student_editor_email.text.toString().trim()
            val name = et_student_editor_name.text.toString().trim()

            if (email.isNotEmpty() && name.isNotEmpty()) {
                val studentBody = StudentBody(email, name)

                if (selectedStudent == null) {
                    editorPresenter.addStudent(studentBody)
                } else {
                    editorPresenter.editStudent(selectedStudent?.id?: return@setOnClickListener, studentBody)
                }
            }
        }
    }

    override fun onAddStudentSuccess() {
        Toast.makeText(this, getString(R.string.msg_success), Toast.LENGTH_SHORT).show()
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun onAddStudentFailure(errMessage: String) {
        Toast.makeText(this, errMessage, Toast.LENGTH_SHORT).show()
    }

    override fun onAddStudentLoading(isLoading: Boolean) {
        if (isLoading) {
            progressDialog.show()
        } else {
            progressDialog.dismiss()
        }
    }
}
